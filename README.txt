TeXLive
=======

Example usage 
-------------

Run "invoice somefile.tex", where "invoice" is:

    #!/bin/bash

    _path=$(realpath $(dirname $1));
    _file=$(basename $1);

    docker run -v $_path:/data -t -i 0x0398/texlive2017 cleanpdf $_file


"cleanpdf" is a helper script, running pdflatex first, then running the
generated PDF through GhostScript to optimize some stuff.
